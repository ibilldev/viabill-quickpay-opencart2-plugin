<?php
class ControllerPaymentQuickPayViabill extends Controller {
	public function index() {
    	$this->language->load('payment/quickpay_viabill');
		
		$this->load->model('checkout/order');

		$data['url']= HTTP_SERVER;

		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_start_date'] = $this->language->get('text_start_date');
		$data['text_issue'] = $this->language->get('text_issue');
		$data['text_wait'] = $this->language->get('text_wait');
		$data['text_creditcard_creditcard'] = $this->language->get('text_creditcard_creditcard');
		$data['text_method_creditcard'] = $this->language->get('text_method_creditcard');
		
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_back'] = $this->language->get('button_back');
		
		$data['action'] = 'https://payment.quickpay.net/';
		
		$fields = array(
						'quickpay_viabill_merchant', 
                        'quickpay_viabill_agreement',
						'quickpay_viabill_language', 
						'quickpay_viabill_currency', 
						'quickpay_viabill_order_status_id', 
						'quickpay_viabill_autocapture', 
                        'quickpay_viabill_autofee'
                        );
		
		foreach ($fields as $field) {
			$data[$field] = $this->config->get($field);
		}
		
		$data['quickpay_viabill_continueurl'] = $this->url->link('checkout/success', '', 'SSL');
		$data['quickpay_viabill_cancelurl'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['quickpay_viabill_callbackurl'] = $this->url->link('payment/quickpay_viabill/callback', '', 'SSL');
		$data['quickpay_viabill_version'] = 'v10';
		
		$data['back'] = $data['quickpay_viabill_cancelurl'];
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$data['order_id'] = str_pad($this->session->data['order_id'], 4, '0', STR_PAD_LEFT);
		$data['amount'] = 100 * $this->currency->format($order_info['total'], $order_info['currency_code'], '', FALSE);
        $data['currency'] = $order_info['currency_code'];
        
		$data['quickpay_cardtypelock'] = $this->get_setting('quickpay_viabill_cardtypelock', 'viabill'); 
        
		$data['quickpay_checksum'] = $this->createMd5($data);

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/quickpay_viabill.tpl')) {
			return $this->load->view( $this->config->get('config_template') . '/template/payment/quickpay_viabill.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/quickpay_viabill.tpl', $data);
		}	
		
	}
    
  	/**
	* is_authorized_callback function.
	*
	* Performs a check on payment callbacks to see if it is legal or spoofed
	*
	* @access public
	* @return boolean
	*/  
    public function is_authorized_callback( $response_body ) 
    {
        if( ! isset( $_SERVER["HTTP_QUICKPAY_CHECKSUM_SHA256"] ) ) 
        {
            return FALSE;
        }
            
        return hash_hmac( 'sha256', $response_body, $this->config->get('quickpay_viabill_privatekey') ) == $_SERVER["HTTP_QUICKPAY_CHECKSUM_SHA256"];
    }

	public function callback() {
        $this->log->write('callback reached');
		$this->load->language('payment/quickpay_viabill');
        
        $callback_body = file_get_contents("php://input");
        $callback_json = json_decode( $callback_body );   

        $order_id = intval($callback_json->order_id);

        if( $order_id > 0 ) {
            $this->load->model('checkout/order');
            $order_info = $this->model_checkout_order->getOrder($order_id);
            $transaction   = end( $callback_json->operations );
            
			if ($this->is_authorized_callback( $callback_body ) ) {
                if( $callback_json->accepted ) {
                    if( 'authorize' == $transaction->type OR 'capture' == $transaction->type) {
                        $this->log->write("Order_id = $order_id. Approved.");
                        $this->model_checkout_order->addOrderHistory(intval($order_id), $this->config->get('quickpay_viabill_order_status_completed'));
                    }
                } else {
                    $this->log->write( 'The transaction for order #' . $order_id . ' was not accepted');
                }
			} else {
				$this->log->write("Order_id = $order_id. Checksum doesn't match.");
			}
		} else {
			$this->log->write('No order found!');
		}
	}

	protected function createMd5($data) {
        $params = array(
            'agreement_id'      => $data['quickpay_viabill_agreement'],
            'merchant_id'       => $data['quickpay_viabill_merchant'],
            'language'          => $data['quickpay_viabill_language'],
            'order_id'          => $data['order_id'],
            'amount'            => $data['amount'],
            'currency'          => $data['currency'],
            'continueurl'       => $data['quickpay_viabill_continueurl'],
            'cancelurl'         => $data['quickpay_viabill_cancelurl'],
            'callbackurl'       => $data['quickpay_viabill_callbackurl'],
            'autocapture'       => $data['quickpay_viabill_autocapture'],
            'autofee'           => $data['quickpay_viabill_autofee'],
            'payment_methods'   => $data['quickpay_cardtypelock'],
            'version'			=> $data['quickpay_viabill_version']
        );

        ksort( $params );

        $checksum = hash_hmac("sha256", implode( " ", $params ), $this->config->get('quickpay_viabill_apikey'));
        
        return $checksum;
	}
    
    private function get_setting( $field, $default = '') {
        $data = $this->config->get( $field );
        
        if( ! empty( $data ) ) {
            return $data;
        }
        
        return $default;
    }
}
?>