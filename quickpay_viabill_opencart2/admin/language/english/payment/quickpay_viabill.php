<?php
// Heading
$_['heading_title'] = 'ViaBill Quickpay Payment Option';

// Text 
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Success: You have modified ViaBill Quickpay Checkout account details!';
$_['text_quickpay_viabill'] = '<a onclick="window.open(\'http://viabill.com/\');"><img  src="../catalog/view/quickpay_viabill/viabill_logo.png" alt="ViaBill Quickpay" title="ViaBill Quickpay"  /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_edit'] = 'Edit ' . $_['heading_title'];

// Entry
$_['entry_status'] = 'Status:';
$_['entry_merchant'] = 'Merchant ID:';
$_['entry_language'] = 'Language:';
$_['entry_apikey'] = 'Api Key:';
$_['entry_agreement'] = 'Agreement ID:';
$_['entry_msgtype'] = 'Msgtype:';
$_['entry_autocapture'] = 'Autocapture:';
$_['entry_splitpayment'] = 'Splitpayment:';
//$_['entry_cardtypelock'] = 'Cardtypelock:';
$_['entry_order_status_completed'] = 'Order Status after Completion:';
$_['entry_secret'] = 'Secret Key:';
$_['entry_payment_methods'] = 'Payment Methods:';
$_['entry_autofee'] = 'Auto-fee:';
$_['entry_privatekey'] = 'Private key';

// Tooltips
$_['tooltip_status'] = 'Enable / Disable the payment gateway';
$_['tooltip_merchant'] =  'Type in your Quickpay merchant ID';
$_['tooltip_agreement'] =  'Type in your Quickpay agreement ID';
$_['tooltip_apikey'] =  'Your agreement API key. Found in the Integration tab inside the Quickpay manager.';
$_['tooltip_privatekey'] =  'Your agreement private key. Found in the Integration tab inside the Quickpay manager.';
$_['tooltip_language'] = 'Set the payment window language';
$_['tooltip_msgtype'] = 'Set the msgtype. Recommended: authorize';
$_['tooltip_autocapture'] = 'Automatically auto capture a payment.';
//$_['tooltip_cardtypelock'] = 'Set the cardtype lock.';
$_['tooltip_order_status_completed'] = 'Choose order state on paid orders.';
$_['tooltip_autofee'] = 'When enabled, the fee charged by the acquirer will be calculated and added to the transaction amount';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Quickpay!';
$_['error_merchant'] = 'Merchant:';
$_['error_agreement'] = 'Agreement:';
$_['error_apikey'] = 'API key:';
$_['error_continueurl'] = 'Continueurl:';
$_['error_cancelurl'] = 'Cancelurl:';
$_['error_callbackurl'] = 'Callbackurl:';
$_['error_secret'] = 'Secret:';
$_['error_msgtype'] = 'Msgtype:';
$_['error_cardtypelock'] = 'Cardtypelock:';
$_['error_privatekey'] = 'Private key:';

?>